#!/bin/bash

find -maxdepth 1 -type f -iname "*.jpg" | while read IMG_FILE; do
	if [ -f "${IMG_FILE}_thumbnail" ]; then
		echo "${IMG_FILE}_thumbnail file exist"
	else
		echo "$IMG_FILE"
		convert "$IMG_FILE" -resize 360 "${IMG_FILE}_thumbnail"
fi
done
